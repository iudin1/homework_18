﻿// homework_18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
using namespace std;

class PlayersData
{
private:

	int Score;
	string PlayerName;
public:
	PlayersData() : PlayerName("Un"),Score (0)
	{}

	PlayersData(string _PlayerName,int _Score) : PlayerName(_PlayerName), Score(_Score)
	{}

	void Show()
	{
		cout << '\n' << PlayerName << ":" << Score << endl;
	}
	
	int GetScore()
	{
		return Score;
	}

	void SetScore(int newScore)
	{
		Score = newScore;
	}

	string GetName()
	{
		return PlayerName;
	}

	void SetName(string newName)
	{
		PlayerName = newName;
	}

	void SetPlayer(string newName, int newScore)
	{
		PlayerName = newName;
		Score = newScore;
	}
};

void SelectionSort(PlayersData* base, int size)
{
	int j;
	PlayersData tmp;
	for (int i = 0; i < size; i++)
	{
		j = i;
		for (int k = i; k < size; k++)
		{
			if (base[j].GetScore() > base[k].GetScore())
			{
				j = k;
			}
		}
		tmp = base[i];
		base[i] = base[j];
		base[j] = tmp;
	}
}

int main()
{
	cout << "How many players" << endl;

	int HowManyPlayers(0);
	cin >> HowManyPlayers;

	string arrName;
	int arrScore;

	PlayersData* base = new PlayersData[HowManyPlayers];

	for (int i = 0, j = 0; i < HowManyPlayers; ++i, j++)
	{
		string y;
		int x;

		cout << "Enter" << y << "score:";
		cin >> x;
		arrScore = x;

		base[i].SetPlayer(arrName, arrScore);
	}

	cout << "\n You enter: \n";
	for (int i = 0; i < HowManyPlayers; i++)
	{
		base[i].Show();
	}

	SelectionSort(base, HowManyPlayers);
	cout << endl;

	for (int i = 0; i < HowManyPlayers; i++)
	{
		base[i].Show();
	}

	return 0;
}



// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
